![logoreadme](compass.png)

![GitLab last commit](https://img.shields.io/gitlab/last-commit/henriquebertolincosta/projetogit?gitlab_url=https%3A%2F%2Fgitlab.com)

## Sprint 01 - Processos Ágeis e Fundamentos de Teste

### Olá esse projeto serve como uma espécie de "caderno" para anotação dos meus aprendizados como bolsista na Compass.UOL

#### Entre os assuntos abordados na Sprint 01, encontram-se:

#### [Dia 01:](https://gitlab.com/henriquebertolincosta/projetogit/-/tree/main/dia_01)
- Onboard: orientações sobre o programa de bolsas e sprints;
- Estudo de git e gitlab;
- Matriz de Eisenhower.

#### [Dia 02:](https://gitlab.com/henriquebertolincosta/projetogit/-/tree/main/dia_02)
- Scrum e manifesto ágil;
- Sprint planning;
- Comunicação nos projetos.

#### [Dia 03:](https://gitlab.com/henriquebertolincosta/projetogit/-/tree/main/dia_03)
- Continuação do Scrum - papéis e responsabilidades.

#### [Dia 04:](https://gitlab.com/henriquebertolincosta/projetogit/-/tree/main/dia_04)
- Fundamentos do teste de software;
- Como um QA pode agregar valor a um projeto?

#### [Dia 05:](https://gitlab.com/henriquebertolincosta/projetogit/-/tree/main/dia_05)
- Continuação dos fundamentos do teste de software, dessa vez mais voltada para o back-end;
- Pirâmide de testes.

#### [Dia 06:](https://gitlab.com/henriquebertolincosta/projetogit/-/tree/main/dia_06)
- Myers e o princípio de Pareto.

#### [Dia 07:](https://gitlab.com/henriquebertolincosta/projetogit/-/tree/main/dia_07)
- Curso de Java - CursoEmVídeo.

#### [Dia 08:](https://gitlab.com/henriquebertolincosta/projetogit/-/tree/main/dia_08)
- Continuação do curso de Java - CursoEmVídeo;
- [Exercícios Beecrowd](https://gitlab.com/henriquebertolincosta/projetogit/-/tree/main/dia_08/beecrowd)

#### [Dia 09:](https://gitlab.com/henriquebertolincosta/projetogit/-/tree/main/dia_09)
- Fundamentos de CyberSecurity e segurança nas redes;
- Webinários da Compass à respeito de CyberSec.

#### Dia 10:
- Apresentação das challenges da Sprint 01.

#### Observação: agradecimento aos colegas William e Guilherme Machado pelas ajudas.

